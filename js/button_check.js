//nasłuchiwanie zmiany stanu przycisków typu radio
var rButtons = document.getElementsByName("basemap");
for (i = 0; i < rButtons.length; i++) {
    rButtons[i].addEventListener("change", function(ev){
        var x = ev.target.id;
        baseLayers[x].bringToFront();
            
        //przesuniecie na wierzch włączonych warstw dodatkowych
        for (i = 0; i < overLayers.length; i++){
            var n = i + rawBaseLen;
            if (document.getElementById(n).checked){
                overLayers[i].bringToFront();
            }
        }
    });
}
//nasłuchiwanie zmiany stanu przycisków typu checkbox
var cButtons = document.getElementsByName("overlayer")
for (i = 0; i < cButtons.length; i++) {
    cButtons[i].addEventListener("change", function(ev){
        var x = ev.target.id;
        if (!ev.target.checked){
            layers[x].bringToBack();
        }
        else if (ev.target.checked){
            layers[x].bringToFront();
        }
    });
}