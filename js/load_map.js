
var url = "js/layers.json";
readJSON(url);

var center = data.center;
var zoom = data.zoom;
var baseLayers = [];
var overLayers = [];

var label1 = "<p> warstwy bazowe:"
document.getElementById("panel").innerHTML += label1;

//iteracja po warstwach bazowcyh
var rawBaseLen = data.basemaps.length;

for (i = 0; i < rawBaseLen; i++) {
    baseLayers.push(L.tileLayer(data.basemaps[i].url, {
        attribution: data.basemaps[i].attribution,
        name: data.basemaps[i].name,
        type: data.basemaps[i].type
    }));
    //dodawanie elementów typu radio button do panelu nawigacyjnego
    var radiohtml = "<input type='radio' name='basemap' id="+i+">"+data.basemaps[i].name+"<br>";
    document.getElementById("panel").innerHTML += radiohtml;
}

var label2 = "<p> warstwy dodatkowe:"
document.getElementById("panel").innerHTML += label2;

//iteracja po warstwach dodatkowe
var rawLayLen = data.layers.length;

for (i = 0; i < rawLayLen; i++){
    //zakładam ze wszystkie warstwy ("layers") są typu wms
    overLayers.push(L.tileLayer.wms(data.layers[i].url, {
        attribution: data.layers[i].attribution,
        name: data.layers[i].name,
        format: data.layers[i].format,
        transparent: data.layers[i].transparent,
        type: data.layers[i].type,
        layers: data.layers[i].layers,
    }));
    //dodawanie elementów typu checkbox do panelu nawigacyjnego
    //do id dodano dlugosc poprzedniej listy aby uniknąć powtórzeń
    var n = i + rawBaseLen;
    var checkhtml = "<input type='checkbox' name='overlayer' id=" + n + " checked>" + data.layers[i].name + "<br>";
    document.getElementById("panel").innerHTML += checkhtml;
}

var layers = baseLayers.concat(overLayers);

//stworzenie widoku mapy
var map = L.map('map', {
    center: center,
    zoom: zoom,
    layers: layers
});

document.getElementById(baseLayers.length-1).checked = true;