//wczytywanie pliku json z danymi dotyczacymi map
var readJSON = function(adres){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            return data = JSON.parse(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", adres, false);
    xmlhttp.send();
}